import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ButtonModule } from 'primeng/button';
import { CarouselModule } from 'primeng/carousel';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SpinnerModule } from 'primeng/spinner';
import { InplaceModule } from 'primeng/inplace';
import { TooltipModule } from 'primeng/tooltip';
import { DialogModule } from 'primeng/dialog';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { FileUploadModule } from 'primeng/fileupload';
import { GalleriaModule } from 'primeng/galleria';
import { InputMaskModule } from 'primeng/inputmask';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CardModule } from 'primeng/card';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ToastModule } from 'primeng/toast';
import { PanelModule } from 'primeng/panel';
import { PickListModule } from 'primeng/picklist';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextModule, DropdownModule, ToggleButtonModule, CheckboxModule, EditorModule,
    MultiSelectModule, SelectButtonModule, OrderListModule, ListboxModule } from 'primeng';
import { TreeModule } from 'primeng';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { TreeTableModule } from 'primeng/treetable';
import { TreeNode } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';
import {GMapModule} from 'primeng/gmap';
import {ProgressBarModule} from 'primeng/progressbar';

import { SpinnerComponent } from './_shared/spinner/spinner.component';

@NgModule({
  declarations: [SpinnerComponent],
  imports: [
    CommonModule,
    TabViewModule,
    CalendarModule,
    RadioButtonModule,
    ButtonModule,
    CarouselModule,
    InputTextareaModule,
    SpinnerModule,
    InputTextModule,
    DropdownModule,
    MessagesModule,
    MessageModule,
    InplaceModule,
    TooltipModule,
    DialogModule,
    InputSwitchModule,
    TableModule,
    TreeTableModule,
    ToggleButtonModule,
    ScrollPanelModule,
    FileUploadModule,
    GalleriaModule,
    ConfirmDialogModule,
    InputMaskModule,
    CardModule,
    KeyFilterModule,
    ToastModule,
    DynamicDialogModule,
    AutoCompleteModule,
    PanelModule,
    CheckboxModule,
    EditorModule,
    TreeModule,
    MultiSelectModule,
    PickListModule,
    SelectButtonModule,
    OrderListModule,
    ListboxModule,
    GMapModule,
    ProgressBarModule,
  ],
  exports: [ 
    TabViewModule,
    CalendarModule,
    RadioButtonModule,
    ButtonModule,
    CarouselModule,
    InputTextareaModule,
    SpinnerModule,
    InputTextModule,
    DropdownModule,
    MessageModule,
    InplaceModule,
    TooltipModule,
    DialogModule,
    InputSwitchModule,
    TableModule,
    TreeModule,
    TreeTableModule,
    ToggleButtonModule,
    ScrollPanelModule,
    FileUploadModule,
    GalleriaModule,
    ConfirmDialogModule,
    InputMaskModule,
    CardModule,
    KeyFilterModule,
    ToastModule,
    DynamicDialogModule,
    AutoCompleteModule,
    PanelModule,
    PickListModule,
    CheckboxModule,
    SpinnerComponent,
    GMapModule,
    ProgressBarModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedModule { }
