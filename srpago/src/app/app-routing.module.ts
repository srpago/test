import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapaComponent } from './components/mapa/mapa.component';
import { SininfoComponent } from './components/sininfo/sininfo.component';


const routes: Routes = [
	{path: 'sininfo', component: SininfoComponent },
	{path: 'mapa', component: MapaComponent},
	{path: '**', redirectTo: 'mapa', pathMatch: 'full'},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
