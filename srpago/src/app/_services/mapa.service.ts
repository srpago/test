import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
}) 
export class MapaService {
    public url:string;

    constructor(
        private _http: HttpClient,
    ){
        this.url = environment.url_api;
        //this.url = 'http://localhost:300/';
    }

    getStatesCatalog(){
        return this._http.get<any>(this.url + 'public/mapa/sepomex/states/get');
    }

    getMcpiosCatalog(stateName){
        return this._http.get<any>(this.url + 'public/mapa/sepomex/state/' + stateName + '/mcipios/get');
    }


    getChkImportStatus(){
        return this._http.get<any>(this.url + 'public/gas/info/chkImportStatus');
    }

    getStartImport(){
        return this._http.get<any>(this.url + 'public/gas/import/start');
    }

    updStatusImport() {
        return this._http.get<any>(this.url + 'public/gas/import/updStatus');
    }

    getSearch(mdlBsq){
        const params = mdlBsq;
        return this._http.get<any>(this.url + 'public/mapa/search/get', {params: params});
    }
}