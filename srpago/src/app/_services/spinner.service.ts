import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
}) 
export class SpinnerService {
    loading: boolean = true;

    constructor()  {}


    // Función que ocualta el SPINNER
    ocultar(){
        this.loading = false;
        console.log('Ocultando SPINNER...', this.loading);
    }

    // Función que muesta el SPINNER
    mostrar(){
        this.loading = true;
        console.log('Mostrando SPINNER...', this.loading);
    }
}