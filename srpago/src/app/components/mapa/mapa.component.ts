import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Dropdown  } from 'primeng';

import { SpinnerService } from 'src/app/_services/spinner.service';
import { MapaService } from 'src/app/_services/mapa.service';

// Para trabajar con JQUERY
declare var JQuery:any;
declare var $: any;

declare var google: any;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css'],
  providers: [MapaService]
})
export class MapaComponent implements OnInit {
	public mdlBsq:any = {};
	public lstStates:any = [];
	public lstMcipios:any = [];
	public lstResultados:any = [];

	public msgInfoUsr:any ;
	public options: any;
	public puntos_obtenidos: any[];

	public banWnImport:boolean = true;
	public banImportWorking:boolean = false;

	public valComplete: number = 0;
	public timer:any ;

	public mapa: any;

	constructor(
			private mapaSvc: MapaService,
			private spinner: SpinnerService,
			private router: Router,
		) { }

	ngOnInit(): void {
		this.options = {
            center: {lat: 19.3910038, lng: -99.2837007},
            zoom: 10
        };
		this.getStatesCatalog();
		this.chkStatusInfo();
		this.mdlBsq.tipo_gasolina = 'regular';
	}

	chkStatusInfo(){
		this.mapaSvc.getChkImportStatus().subscribe(
			r => {
				if(r.results){
					if(r.results.complete == 100)
						this.banWnImport = false;
				}
				if(r.message){
					if(r.message== 'Importación en proceso')
						this.updStatusImport(true);
					this.msgInfoUsr = r.message;
					this.muestraErr(null);
				}
				console.log(r);
			},
			e => {
				this.muestraErr(e);
			}
		);
	}

	startImport(){
		this.mapaSvc.getStartImport().subscribe(
			r => {
				if(r.success){
					this.banImportWorking = true;
					this.timer = setInterval(() => { this.updStatusImport(); }, 3000);
				}
			},
			e => {
				this.muestraErr(e);
			}
		);
	}

	updStatusImport(initInterval=false){
		if(initInterval)
			this.timer = setInterval(() => { this.updStatusImport(); }, 3000);
		this.mapaSvc.updStatusImport().subscribe(
			r => {
				if(r.results){
					if(r.results.complete === 100){
						console.log('migración completada');
						this.banImportWorking = false;
						this.banWnImport = false;
						clearInterval(this.timer);
					}else{
						this.valComplete = r.results.complete;
						this.banImportWorking = true;
					}
				}
			}
		);
	}

	getStatesCatalog(){
		this.spinner.mostrar();
		this.mapaSvc.getStatesCatalog().subscribe(
			r=> {
				this.lstStates = r.results;
				/*if(r.message){
					this.msgInfoUsr = r.message;
					this.muestraErr(null);
				}*/
				this.spinner.ocultar();
			},
			e => {
				this.muestraErr(e);
			}
		);
	}

	chgState(){
		this.getMcpiosCatalog();
	}

	getMcpiosCatalog(){
		this.spinner.mostrar();
		this.mapaSvc.getMcpiosCatalog(this.mdlBsq.state).subscribe(
			r => {
				this.lstMcipios = r.results;
				/*if(r.message){
					this.msgInfoUsr = r.message;
					this.muestraErr(null);
				}*/
				this.spinner.ocultar();
			},
			e => {
				this.muestraErr(e);
			}
		);
	}

	realizaBsq(){
		this.spinner.mostrar();
		this.puntos_obtenidos = null;
		this.puntos_obtenidos = [];
		this.mapaSvc.getSearch(this.mdlBsq).subscribe(
			r => {
				r.results.forEach((row) => { this.addMarker(row);})
				this.lstResultados = r.results;
				this.spinner.ocultar();
			},
			e => {
				this.muestraErr(e);
			}
		);
	}

	addMarker(obj){
		console.log('creando Marcador => ' +  obj.razonsocial + ' ||   Lat: ' + obj.latitude + ' ||  Lng: ' + obj.longitude + ' || CP: ' + obj.codigopostal);
		let bounds = new google.maps.LatLngBounds();
		this.puntos_obtenidos.push(new google.maps.Marker({position: {lat: obj.latitude, lng: obj.longitude}, title: obj.razonsocial}))
		this.puntos_obtenidos.forEach(mkr => {
            bounds.extend(mkr.getPosition());
        });

        setTimeout(()=> {
            this.mapa.fitBounds(bounds);
        }, 1000);
	}

	setMap(event) {
        this.mapa = event.map;
    }

	/*********************************************
	**
	**   MANEJO DE ERRORES
	**
	**********************************************/

  	muestraErr(e:any){
  		this.spinner.ocultar();
	  	console.log('Mostrando error...');
	  	console.log(e !== null? e : this.msgInfoUsr);
	  	if(e !== null){
	  		if(e.status){
	  			if(e.status === 403){
	  				this.router.navigate(['/login']);
	  				this.expiroSesion();
	  				return;
	  			}
	  		}
	  		if(e.error.message && e.error.message != '')
	  			this.msgInfoUsr = e.error.message;
	  		else
	  			if(e.error.errors) {
	  				let llaves = Object.keys(e.error.errors);
	  				this.msgInfoUsr = '<ul>';
	  				llaves.forEach(obj => {
	  					this.msgInfoUsr += '<li><br><strong>' + obj + '</strong><ul>';
	  					Object.keys(e.error.errors).forEach(key => {
	  						if(obj == key){
	  							this.msgInfoUsr += '<li>' + e.error.errors[key] + '</li>';
	  						}
	  					});
	  					this.msgInfoUsr += '</ul></li>';
	  				});
	  				this.msgInfoUsr += '</ul>';
	  				//let txtHTML  = document.getElementById("msgInfo_html").textContent;
	  			}
  		}
  		$('#msgInfo_html').empty().append(this.msgInfoUsr);
  		$('#wnMsgInfo').modal('show');
  	}

  	expiroSesion(){
	    $(`<div class="modal fade" id="wnExpiro" role="dialog"> 
	       <div class="modal-dialog modal-dialog-centered"> 
	         <!-- Modal content--> 
	          <div class="modal-content"> 
	             <div class="modal-body" style="padding:10px;"> 
	               <h4 class="text-center">Expiro su sesión, es necesario que ingrese su usuario y contraseña nuevamente</h4> 
	               <div class="text-center"> 
	                 <a class="btn btn-warning btn-aceptar">Aceptar</a>
	               </div> 
	             </div> 
	         </div> 
	      </div> 
	    </div>`).appendTo('body');

	    $("#wnExpiro").on('hidden.bs.modal', function () {
	        $("#wnExpiro").remove();
	     });

	    $("#wnExpiro").modal({
	       backdrop: 'static',
	       keyboard: false
	    });

	    $(".btn-aceptar").click(function () {
	         $("#wnExpiro").hide();;
	     });
	}

}
