# PRUEBA TÉCNICA PARA VACANTE EN PHP EN SRPAGO

Ejercicio solicitado por RRHH para poder participar para la vacante de PHP 
---
**NOTA IMPORTANTE**

> Los supuestos son que tanto MySQL, como PHP (y sus dependencias) ya se encuentran instalados en la máquina local
> * php-curl debe estar instalada 
---


>## TECNOLOGÍAS OCUPADAS

>### FRONTEND
>* Angular 10
>* Prime 10
>* BOOTSTRAP 4
>* HTML5
>* CSS3
>* Javascript y TypeScript
>* Template ocupado [SBADMIN2](https://startbootstrap.com/themes/sb-admin-2/)

>### BACKEND
>* SlimPHP
>* FluentPDO
>* Curl

## Clonación de repositorio

```console
$ mkdir lgonzalez
$ cd lgonzalez
$ git clone git@gitlab.com:srpago/test.git
$ cd test
```

El proyecto creara la siguiente estructura de archivos
```console
.
├── bknd_srpago
│   ├── 01_CONFIG
│   │   ├── DB
│   │   └── DEPENDENCIES
│   ├── app
│   │   ├── lib
│   │   ├── middleware
│   │   ├── model
│   │   ├── route
│   │   └── scripts
│   ├── logs
│   ├── public
│   ├── src
│   ├── templates
│   └── vendor
│       ├── composer
│       ├── container-interop
│       │   └── container-interop
│       │       ├── docs
│       │       │   └── images
│       │       └── src
│       │           └── Interop
│       │               └── Container
│       │                   └── Exception
│       ├── firebase
│       │   └── php-jwt
│       │       ├── src
│       │       └── tests
│       ├── lichtner
│       │   └── fluentpdo
│       │       ├── FluentPDO
│       │       └── tests
│       ├── monolog
│       │   └── monolog
│       │       ├── doc
│       │       ├── src
│       │       │   └── Monolog
│       │       │       ├── Formatter
│       │       │       ├── Handler
│       │       │       │   ├── Curl
│       │       │       │   ├── FingersCrossed
│       │       │       │   └── SyslogUdp
│       │       │       └── Processor
│       │       └── tests
│       │           └── Monolog
│       │               ├── Formatter
│       │               ├── Handler
│       │               │   └── Fixtures
│       │               └── Processor
│       ├── nikic
│       │   └── fast-route
│       │       ├── src
│       │       │   ├── DataGenerator
│       │       │   ├── Dispatcher
│       │       │   └── RouteParser
│       │       └── test
│       │           ├── Dispatcher
│       │           └── RouteParser
│       ├── pimple
│       │   └── pimple
│       │       ├── ext
│       │       │   └── pimple
│       │       │       └── tests
│       │       └── src
│       │           └── Pimple
│       │               └── Tests
│       │                   └── Fixtures
│       ├── psr
│       │   ├── http-message
│       │   │   └── src
│       │   └── log
│       │       └── Psr
│       │           └── Log
│       │               └── Test
│       └── slim
│           ├── php-view
│           │   ├── src
│           │   └── tests
│           └── slim
│               ├── example
│               └── Slim
│                   ├── Exception
│                   ├── Handlers
│                   │   └── Strategies
│                   ├── Http
│                   └── Interfaces
│                       └── Http
└── srpago
    ├── e2e
    │   └── src
    └── src
        ├── app
        │   ├── components
        │   │   ├── mapa
        │   │   └── sininfo
        │   ├── _services
        │   └── _shared
        │       └── spinner
        ├── assets
        │   ├── css
        │   ├── fontawesome-free
        │   │   ├── css
        │   │   ├── js
        │   │   ├── less
        │   │   ├── metadata
        │   │   ├── scss
        │   │   ├── sprites
        │   │   ├── svgs
        │   │   │   ├── brands
        │   │   │   ├── regular
        │   │   │   └── solid
        │   │   └── webfonts
        │   ├── jquery-easing
        │   └── js
        └── environments
```


## INSTALACIÓN DE APLICACIÓN 

### Permisos necesarios
Se deben otorgar permisos para el usuario de apache, para escribir en la carpeta LOGS 
Y se deben dar perisos de ejecución para el SCRIPT_IMPORTACIÓN al usuario de APACHE
	
```console
sudo chmod -R 777 RUTA_CLONACION/bknd_srpago/logs 
sudo chown www-data. RUTA_CLONACION/bknd_srpago/app/scripts/import_precios.php
sudo chmod 755 RUTA_CLONACION/bknd_srpago/app/scripts/import_precios.php
```


### Creación de base de datos
Cambiarse a la carpeta **./bknd_srpago/01_CONFIG/DB** ahí encontrará el archivo **create_db_srpago.sql** el cuál debera ejecutar con un SUPER USUARIO (root) de MySQL

```console
$ cd RUTA_CLONACION/bknd_srpago/01_CONFIG/DB/
$ mysql -u root -p < create_db_srpago.sql
```

### Creación de VIRTUAL HOST

Cambiarse a la carpeta **./bknd_srpago/01_CONFIG/** en donde se encuentra el archivo de configuración **vh_bknd_srpago.conf**

El mismo debe colocarse en **/etc/apache2/sites-available/**
```console
$ cd test/bknd_srpago/01_CONFIG
$ sudo cp vh_bknd_srpago.conf /etc/apache2/sites-available/
```
Crear enlace simbólico en **/etc/apache2/sites-enabled/**

```console
$ sudo cd /etc/apache2/sites-enabled
$ sudo ln -s ../sites-available/vh_bknd_srpago.conf .
```

Cambiarse al directorio **/var/www/**
Crear enlace simbólico a la ruta del proyecto en **./bknd_srpago/public**
```console
$ sudo cd /var/www/
$ sudo ln -s RUTA_CLONACION/bknd_srpago/public
```

Reiniciar servicio de APACHE
```console
$ sudo service apache2 restart
```

Esto habilita el puerto 200 de la maquina, se puede probar iniciando un explorador de internet (browser) e ingresando la URL http://localost:200/ la cual debe regresar el logo de SRPAGO si esto no ocurre algo se ha configurado mal


## Ejecución de aplicación ANGULAR

Cambiarse a la carpeta 
```console
$ cd RUTA_CLONACION/srpago/
```

Instalar todas las dependencias de ANGULAR
```console
$ npm i 
```
Ejecutar servidor de desarrollo de ANGULAR
```console
$  npm start
```

Esto permitirá que la aplicación se ejecute a través del puerto 4200, para ver la aplicación se debe ingresar la URL http://localhost:4200/

Listo...!!!!!








