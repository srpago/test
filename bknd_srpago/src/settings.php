<?php
return [
    'settings' => [
        'displayErrorDetails' => true,

        // Configuración de rendereo
        'renderer' => [
            'template_path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR,
        ],

        // Configuración de MONOLOG
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'app.log',
        ],
        
        // Configuración de la aplicación sobre SLIMPHP
        'app_token_name'   => 'APP-TOKEN',
        'connectionString' => [
            'dns'  => 'mysql:host=localhost;dbname=db_srpago;charset=utf8',
            'user' => 'usr_srpago',
            'pass' => 'B4s3-SrP4g0!!!'
        ]
    ],
    'script_import' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'import_precios.php',
];
