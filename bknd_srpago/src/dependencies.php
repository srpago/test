<?php


$container = $app->getContainer();

/* DEFINIMOS MANEJO DE ERRORES
**
**  PARA QUE EL USUARIO NO VEA LOS ERRORES, DEBEMOS MANEJAR LOS ERRORES DE PHP
**
**
*/
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withJson(['message'=>'La ruta que esta buscando no existe. Por favor, verifique.'],404);
    };
};

$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $response->withJson(['message' => 'El tipo de método debe ser: ' . implode(', ', $methods)],405);
    };
};

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        error_log('====================  ERROR 500  ====================');
        error_log('Err_code: ' . $exception->getCode());
        error_log('Err_arch: ' . $exception->getFile());
        error_log('Err_Msg: ' . $exception->getMessage());
        error_log('Err_lin: ' . $exception->getLine());
        error_log('Err_trace: ' . $exception->getTraceAsString());
        error_log('--------------------  ERROR 500  --------------------');
        return $response->withJson(['message' => 'Algo ha ido mal!!! Nuestro equipo de soporte lo resolverá lo antes posible.'],500);
    };
};

$container['phpErrorHandler'] = function ($c) {
    return function ($request, $response, $error) use ($c) {
        error_log('====================  ERROR 500 - PHP_RunTime_Err  ====================');
        error_log('Err_code: ' . $error->getCode());
        error_log('Err_arch: ' . $error->getFile());
        error_log('Err_Msg: ' . $error->getMessage());
        error_log('Err_lin: ' . $error->getLine());
        error_log('Err_trace: ' . $error->getTraceAsString());
        error_log('--------------------  ERROR 500 - PHP_RunTime_Err  --------------------');
        return $response->withJson(['message' => 'Error en tiempo de ejecución. Nuestro equipo ya esta trabajando para solucionar este problema.'],500);
    };
};

// Rendereamos la vista
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// Instanciamos MONOLOG
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Configuramos base de datos a MYSQL
// TODO: Se debe cambiar el usuario ROOT por un usuario sin más privilegios que los necesarios para la base de datos 
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];
    
    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    $pdo->exec('set session sql_mode = traditional');
    //$pdo->exec('set session innodb_strict_mode = on')
    //PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="TRADITIONAL"'

    $pdo->debug = true;

    $pdo->debug = function($BaseQuery) {
        echo "query: " . $BaseQuery->getQuery(false) . "<br/>";
        echo "parameters: " . implode(', ', $BaseQuery->getParameters()) . "<br/>";
        echo "rowCount: " . $BaseQuery->getResult()->rowCount() . "<br/>";  
    };

    return new FluentPDO($pdo); 
};

// Instanciamos los MODELOS ocupados en el proyecto
$container['model'] = function($c){
    return (object)[
        'sepomex'       => new App\Model\SepomexModel($c->db),
        'gas'           => new App\Model\PrecioGasolinaModel($c->db),
        'mapa'          => new App\Model\MapaModel($c->db),
    ];
};