<?php
// Declaración de RUTAS


// Agregamos RUTA base para mostrar logo de SRPAGO
$app->get('/[{name}]', function ($request, $response, $args) {
    $this->logger->info("Slim-Skeleton '/' route");

    // Rendereamos TEMPLATE index
    return $this->renderer->render($response, 'index.phtml', $args);
});
    