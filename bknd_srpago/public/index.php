<?php
// Eliminamos problema de CROSS-ORIGIN
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}
if (PHP_SAPI == 'cli-server') {
    // Validamos que la petición realizada al servidor no sea un archivo físico
    // Si es un archivo físico no hacemos nada y dejamos al servidor responder normalmente
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}
// Cargamos todas las librerías ocupadas en el proyecto
require __DIR__ . '/../vendor/autoload.php';

// Instanciamos la aplicación de SLIMPHP
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Cargamos todos los archivos de la aplicación separadas según objetivo
require __DIR__ . '/../app/app_loader.php';

// Configuramos las dependencias
require __DIR__ . '/../src/dependencies.php';

// Registramos el middleware
require __DIR__ . '/../src/middleware.php';

// Registramos las rutas a ocupar
require __DIR__ . '/../src/routes.php';


// Ejecutamos la aplicación
$app->run();
