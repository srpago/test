<?php
namespace App\Model;

use App\Lib\Response;

class SepomexModel
{
    private $db;
    private $table = 'sepomex';
    private $response;
    
    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }


    /*************************************
    **
    **  Devuelve el catálogo necesario para llenar el combo de estados
    **
    **
    **************************************/

    public function getSepomexStatesCatalog(){
        $qry = $this->db->from($this->table)->select(null)
        ->select('DISTINCT d_estado as value, d_estado as label')
        ->orderBy('d_estado');
        return $this->response->SetResponse(true, 'Catálogo SEPOMEX::ESTADOS obtenido con éxito', $qry->fetchAll());
    }

    /*************************************
    **
    **  Devuelve el catálogo necesario para llenar el combo de municipios y esta en función del estado seleccionado
    **
    **  @params 
    **      string $stateName Nombre del estado seleccionado
    **************************************/
    public function getSepomexMcipiosCatalog($stateName){
        $qry = $this->db->from($this->table)->select(null)
        ->select('DISTINCT d_mnpio as value, d_mnpio as label')
        ->where('d_estado', $stateName)
        ->orderBy('d_mnpio');
        return $this->response->SetResponse(true, 'Catálogo SEPOMEX::MUNICIPIOS obtenido con éxito', $qry->fetchAll());
    }
}