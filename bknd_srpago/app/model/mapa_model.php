<?php
namespace App\Model;

use App\Lib\Response;

class MapaModel
{
    private $db;
    private $response;
    
    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    /*************************************
    **
    **  Realiza la búsqueda de todos los puntos relacionados a los parámetros enviados
    **
    **  @params 
    **      string $stateName Nombre del estado seleccionado
    **      string $mcpioName Nombre del municipio seleccionado
    **
    **
    **  RETURN JSON
    **      {
    **          _id => ID en base64 obtenido de servicio de preicos de gasolina
    **          rfc => RFC
    **          razonsocial => Razón social
    **          date_insert => Fecha de inserción
    **          numeropermiso => Número de permiso
    **          fechaaplicacion => Fecha de aplicación
    **          permisoid       => No de permiso otorgado
    **          longitude       => Longitud geografica
    **          latitude        => Latitud geografica
    **          codigopostal    => Código postal
    **          calle           => Dirección obtenida
    **          colonia         => Colonia
    **          municipio       => Municipio
    **          estado          => Entidad federativa
    **          regular         => Precio de gasolina REGULAR
    **          premium         => Precio de gasolina PREMIUM
    **          dieasel         => Precio de gasolina DIEASEL
    **      }
    **************************************/
    public function getSearch($stateName, $mcpioName, $ordenamiento = 1, $tipo_gasolina = 'regular'){
        $qry = $this->db->from('precio_gasolina pg ')->disableSmartJoin()->select(null)
                ->innerJoin("
                    (SELECT DISTINCT d_codigo FROM sepomex WHERE d_estado = '" . utf8_decode($stateName) . "' AND d_mnpio= '" . utf8_decode($mcpioName) . "') s ON (s.d_codigo = pg.codigopostal)")
                ->select('pg._id as id, pg.rfc, pg.razonsocial, pg.date_insert, pg.numeropermiso')
                ->select(' pg.fechaaplicacion, pg.permisoid, pg.longitude, pg.latitude, pg.codigopostal')
                ->select(' pg.calle')
                ->select(' pg.regular, pg.premium, pg.dieasel');
        switch ($ordenamiento) {
            case 2:
                $order = " DESC";
                break;
            default:
                $order = " ASC";
                break;
        }

        switch ($tipo_gasolina) {
            case 'premium':
                $qry->orderBy(' pg.premium ' . $order);
                break;
            case 'dieasel':
                $qry->orderBy(' pg.dieasel ' . $order);
                break;
            
            default:
                $qry->orderBy(' pg.regular ' . $order);
                break;
        }
        return $this->response->SetResponse(true, 'puntos obtenidos con éxito', $qry->fetchAll());
    }
}