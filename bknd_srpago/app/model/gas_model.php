<?php
namespace App\Model;

use App\Lib\Response;

class PrecioGasolinaModel
{
    private $db;
    private $table = 'precio_gasolina';
    private $response;
    
    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }


    /*************************************
    **
    **  Valida la cantidad de registros guardados contra los obtenidos del API RestFul
    ** https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=1
    **
    ** @PARAMS $num_regs => Total de registros obtenido de petición a API RestFul
    ** @RETURN array
    **
    **************************************/

    public function chkImportStatus($num_regs){

        // Obtenemos el COUNT de la tabla
        $obj = $this->db->from($this->table)->select(null)
        ->select('COUNT(id) as total')->fetch();

        // Validamos que los números enviados por el servicio sean iguales al COUNT de la tabla
        if($obj->total == $num_regs){
            return $this->response->SetResponse(true,'Importación realizada con éxito', 
                [
                    'imported_records'  => $obj->total, 
                    'complete'          => 100
                ]
            );
        }
        else{
            // SI NO ...
            sleep(1);
            // Si el total del COUNT es igual a 0
            // Esperamos un segundo y volvemos a validar, si la respusta es  la misma el proceso de importación no ha empezado
            if($obj->total < 1){
                $obj = $this->db->from($this->table)->select(null)->select('COUNT(id) as total')->fetch();
                if($obj->total == 0){
                    error_log('total es igual a cero');
                    return $this->response->SetResponse(
                        true, 
                        'Importación no iniciada', 
                        [
                            'status' => 'sin iniciar'
                        ]
                    );
                }
            }else{
                $respuesta = [
                        'imported_records'  => $obj->total, 
                        'records_to_import' => $num_regs - $obj->total,
                        'complete'          => round(($obj->total * 100) / $num_regs,0)
                    ];
                error_log(print_r($respuesta,true));
                return $this->response->SetResponse(
                    true,
                    'Importación en proceso', 
                    $respuesta
                );
            }
        }
    }
}