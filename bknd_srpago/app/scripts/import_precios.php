#!/usr/bin/env php
<?php

class ImportaPrecios {
 
    const DB_HOST = 'localhost';
    const DB_NAME = 'db_srpago';
    const DB_USER = 'usr_srpago';
    const DB_PASSWORD = 'B4s3-SrP4g0!!!';
 
    public function __construct() {
        $conStr = sprintf("mysql:host=%s;dbname=%s;charset=utf8", self::DB_HOST, self::DB_NAME);
        try {
            $this->pdo = new PDO($conStr, self::DB_USER, self::DB_PASSWORD);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo "\n" . $e->getMessage() . "\n";
            die("\n=====  TERMINA PROCESO " . Date('Y-m-d H:i:s') . "  =====\n");
        }
    }
 
    private $pdo = null;

    function verifyZipCode(){
        $qry = "SELECT DISTINCT latitude, longitude FROM precio_gasolina WHERE latitude";
        //$stmt_latlng = $this->pdo->prepare($qry);
    }

    /*****************************************
    **  Función que obtiene e inserta la información a la tabla PRECIO_GASOLINA
    **  @params
    **  @return void
    *****************************************/

    function getPreciosInfo(){
        // URL A CONSULTAR
        $url_precios = 'https://api.datos.gob.mx/v1/precio.gasolina.publico';

        // Obtenemos el total de registros a insertar

        $data_params = [
                    'pageSize'  => 1,
                ];
        $rCurl = json_decode(CurlHelper::perform_http_request('GET',$url_precios, $data_params));
        if(!$rCurl){
            print("\n\n\t\t >>>>>     ERROR DE CONEXIÓN, POR FAVOR VALIDE     <<<<<");
            return;
        }

        // Evaluamos si existe algún error en la petición
        if(isset($rCurl->error_code)){
            // Si existe detenemos proceso
            print('  >>>  PROCESO INTERRUMPIDO <<<');
            return;
        }
        $totRegs = $rCurl->pagination->total;
        $no_pag = 1;
        $pageSize = 1000;
        // Obtenemos todos los campos del arreglo
        $obj = $rCurl->results[0];
        $fields = array_keys(json_decode(json_encode($obj),true));
        
        $totPages = ($totRegs % $pageSize) < ($pageSize / 2) ? round($totRegs / $pageSize) + 1 : round($totRegs / $pageSize,0,PHP_ROUND_HALF_UP); 

        print('TOTPAGES => ' . $totPages . ' || TOTREGS => ' . $totRegs . ' || PAGESIZE => ' . $pageSize);
        for($i = 1; $i <= $totPages; $i++){
            $data_params['pageSize']    = $pageSize;
            $data_params['page']        = $i;
            $rCurl = json_decode(CurlHelper::perform_http_request('GET',$url_precios, $data_params));
            print("\n\t\t ITERACIÓN => " . $i . "\n");
            if(isset($rCurl->error_code)){
                // Si existe detenemos proceso
                print('  >>>  PROCESO INTERRUMPIDO <<<');
                break;
            }


            // MySQL permite sentencias multiples
            // Dado que es una fuente de información confiable creamos una sentencia INSERT customizada para el registro multiple de registros
            // De esa manera optimizamos más la IMPORTACIÓN
            $qry = 'INSERT INTO precio_gasolina (_id, calle, rfc, date_insert, regular, colonia, numeropermiso, fechaaplicacion, permisoid, longitude, latitude, premium, razonsocial, codigopostal, dieasel) VALUES ';
            //(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            foreach ($rCurl->results as $row) {
                $qry .= "(";
                $qry .= "'" . $row->_id . "',";
                $qry .= "'" . str_replace("'", "\'", $row->calle) . "',";
                $qry .= "'" . $row->rfc . "',";
                $qry .= "'" . $row->date_insert . "',";
                $qry .= "'" . floatval($row->regular) . "',";
                $qry .= "'" . $row->colonia . "',";
                $qry .= "'" . $row->numeropermiso . "',";
                $qry .= "'" . $row->fechaaplicacion . "',";
                $qry .= "'" . $row->﻿permisoid . "',";
                $qry .= "'" . $row->longitude . "',";
                $qry .= "'" . $row->latitude . "',";
                $qry .= "'" . floatval($row->premium) . "',";
                $qry .= "'" . str_replace("'", "\'", $row->razonsocial) . "',";
                $qry .= "'" . $row->codigopostal . "',";
                $qry .= "'" . floatval($row->dieasel) . "'";
                $qry .= "),";
                /*$stmt_ins = $this->pdo->prepare($qry);
                try{
                    $stmt_ins->execute(
                        [
                            $row->_id,
                            $row->calle,
                            $row->rfc,
                            $row->date_insert,
                            floatval($row->regular),
                            $row->colonia,
                            $row->numeropermiso,
                            $row->fechaaplicacion,
                            $row->﻿permisoid,
                            $row->longitude,
                            $row->latitude,
                            floatval($row->premium),
                            $row->razonsocial,
                            $row->codigopostal,
                            floatval($row->dieasel)
                        ]
                    );
                }catch(\Exception $e){
                    error_log('ERR => ' . print_r($e,true));
                    error_log('QRY => ' . print_r($row,true));
                }*/
            }
            try{
                $stmt_ins = $this->pdo->prepare(substr($qry, 0, -1));
                $stmt_ins->execute();
            }catch(\Exception $e){
                error_log('ERR => ' . print_r($e,true));
                error_log('QRY => ' . print_r($row,true));
            }
        }
    }

    public function __destruct() {
        $this->pdo = null;
    }
 
};

class CurlHelper {
    public static function perform_http_request($method, $url, $data = false,$headers = array())
    {
        // Inicializamos CURL
        $curl = curl_init();
        // Evaluamos tipo de metódo
        switch ($method)
        {
            case "POST":
                // Si el metódo es POST, debemos enviar los datos
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                // SI es PUT, debemos setear
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                // En caso contrario, los parámetros deben enviarse como parte de la consulta (QUERY_STRING)
                $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Si existen ENCABEZADOS los agregamos
    	if(!empty($headers)) curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Realizamos petición y obtenemos el resultado
        $result = curl_exec($curl);

        // Evaluamos si existe algún error
        if (curl_errno($curl)) {
            // Guardamos LOG
            error_log(print_r(array('error_code' => curl_errno($curl),
                    'error_message' => curl_error($curl),
                    'response' => false
            )));
            // Regresamos error como JSON
            return json_encode(array('error_code' => curl_errno($curl),
                    'error_message' => curl_error($curl),
                    'response' => false
            ));
        }
        curl_close($curl);
        // Si no existe ningún error, regresamos respuesta esperada (JSON)
        return $result;
    }

};

echo "\n=====  INICIA PROCESO " . Date('Y-m-d H:i:s') . "  =====\n";
$obj = new ImportaPrecios();
$obj->getPreciosInfo();

$obj->verifyZipCode();

echo "\n\n=====  TERMINA PROCESO " . Date('Y-m-d H:i:s') . "  =====\n";


?>
