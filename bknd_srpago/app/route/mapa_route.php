<?php
use App\Lib\Response;

/**************************************
**
**	GENERAMOS RUTAS USADAS POR EL FRONTEND 
**
**
***************************************/
$app->group('/api', function() use ($app) {
    $app->group('/v1', function() use ($app) {
        $app->group('/public', function () use ($app){
        	$app->group('/mapa', function(){


                /*************************************
                **
                **  RUTA para obtener el catálogo de estados
                **  METHOD: GET
                **
                **************************************/
                $this->get('/sepomex/states/get', function($req, $res, $args){
                    $r = new Response();
                    $r = $this->model->sepomex->getSepomexStatesCatalog();
                    if(!$r->success)
                        return $res->withJson($r->errors,422, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                    return $res->withJson($r,200, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                });

                /*************************************
                **
                **  RUTA para obtener el catálogo de municipios que esta en función del estado seleccionado
                **  METHOD: GET
                **  
                ** @PARAMS
                **      stateName : Nombre del estado seleccionado
                **
                **************************************/
                $this->get('/sepomex/state/{stateName}/mcipios/get', function($req, $res, $args){
                	$r = new Response();
                	$r = $this->model->sepomex->getSepomexMcipiosCatalog($args['stateName']);
                    if(!$r->success)
                        return $res->withJson($r->errors,422, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                    return $res->withJson($r,200, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                });



                $this->get('/search/get', function($req, $res, $args){
                    $params = $req->getQueryParams();
                    $params = array_map('utf8_encode', $params);
                    $ordenamiento = !isset($params['ordenamiento'])? 2 : $params['ordenamiento'];
                    $tipo_gasolina = !isset($params['tipo_gasolina'])? 'regular' : $params['tipo_gasolina'];
                    $r = new Response();
                    $r = $this->model->mapa->getSearch($params['state'], $params['mcpio'], $ordenamiento, $tipo_gasolina);
                    if(!$r->success)
                        return $res->withJson($r->errors,422, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                    return $res->withJson($r,200, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                });

            });
    	});
    });
});
