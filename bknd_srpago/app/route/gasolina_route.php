<?php
use App\Lib\Response;

/**************************************
**
**	GENERAMOS RUTAS USADAS POR EL FRONTEND 
**
**
***************************************/
$app->group('/api', function() use ($app) {
    $app->group('/v1', function() use ($app) {
        $app->group('/public', function () use ($app){
        	$app->group('/gas', function(){


                /*************************************
                **
                **  RUTA que revisa si la información fue importada y se encuentra completa
                **  METHOD: GET
                **
                **  RETURN JSON
                **    {
                **      'imported_records'  total de registros importandos hasta el momento
                **      'records_to_import' total de registros que faltan importar
                **      'complete'          entero para mostrar avance del progreso
                **    }
                **
                **************************************/
                $this->get('/info/chkImportStatus', function($req, $res, $args){
                    $r = new Response();

                    // Debemos consultar el servicio para conocer el total de registros a importar
                    $cURLCnx = curl_init();
                    curl_setopt($cURLCnx, CURLOPT_URL, 'https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=1');
                    curl_setopt($cURLCnx, CURLOPT_RETURNTRANSFER, true);

                    $lstPrecios = curl_exec($cURLCnx);
                    $code = @curl_getinfo($cURLCnx, CURLINFO_HTTP_CODE);
                    curl_close($cURLCnx);

                    $respuesta = json_decode($lstPrecios);

                    $r = $this->model->gas->chkImportStatus($respuesta->pagination->total);
                    if(!$r->success)
                        return $res->withJson($r,422, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                    return $res->withJson($r,200, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                });

                /*************************************
                **
                **  RUTA para iniciar importación de información
                **  METHOD: GET
                **  
                ** @PARAMS NULL
                **
                **
                **     RETURN JSON
                **      {
                **          message = Mensaje que indica si el script no fue encontrado
                **      }
                **
                **************************************/
                $this->get('/import/start', function($req, $res, $args){
                	$r = new Response();

                    // Validamos conexión a servicio API Restful
                    $cURLCnx = curl_init();
                    curl_setopt($cURLCnx, CURLOPT_URL, 'https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=1');
                    curl_setopt($cURLCnx, CURLOPT_RETURNTRANSFER, true);
                    $cnx = curl_exec($cURLCnx);
                    curl_close($cURLCnx);
                    error_log('Validando existencia SCRIPT => ' . $this->script_import);
                    if(file_exists($this->script_import)){
                	   exec($this->script_import . " >> /tmp/import.log 2>&1 &");
                       $r->SetResponse(true);
                    }
                    else
                        $r->SetResponse(false, 'No se encontró el script de importación.');
                    if(!$r->success)
                        return $res->withJson($r->errors,422, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                    return $res->withJson($r,200, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                });


                /*************************************
                **
                **  RUTA para actualizar el STATUS de importación de información
                **  METHOD: GET
                **  
                **
                **  RETURN JSON
                **    {
                **      'imported_records'  total de registros importandos hasta el momento
                **      'records_to_import' total de registros que faltan importar
                **      'complete'          entero para mostrar avance del progreso
                **    }
                **
                **************************************/


                $this->get('/import/updStatus', function($req, $res, $args){
                    $r = new Response();

                    // Debemos consultar el servicio para conocer el total de registros a importar
                    $cURLCnx = curl_init();

                    curl_setopt($cURLCnx, CURLOPT_URL, 'https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=1');
                    curl_setopt($cURLCnx, CURLOPT_RETURNTRANSFER, true);

                    $lstPrecios = curl_exec($cURLCnx);
                    curl_close($cURLCnx);

                    $respuesta = json_decode($lstPrecios);

                    if(!$respuesta){
                        $code = @curl_getinfo($cURLCnx, CURLINFO_HTTP_CODE);
                        if($code != 200){
                            return $res->withJson(
                                [
                                    'errNo'=> $code, 
                                    'message' => 'Error de conexión al servicio de precios de gasolina'
                                ],
                                422
                            ); 
                        }
                    }

                    $r = $this->model->gas->chkImportStatus($respuesta->pagination->total);
                    if(!$r->success)
                        return $res->withJson($r,422, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                    return $res->withJson($r,200, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
                });
            });
    	});
    });
});
