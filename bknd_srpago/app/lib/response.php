<?php
namespace App\Lib;
// Clase que se encargará de formatear la respuesta, según las indicaciones provistas en el requerimiento
class Response
{
	public $results     = null;
	public $success   = false;
	public $message    = 'Ocurrio un error inesperado.';
    public $errors     = [];
	

	// Función que devuelve las variables solicitadas en requerimiento
	public function SetResponse($success, $m = '', $results = null)
	{
		$this->success = $success;
		$this->message = $m;
		$this->results = $results;
        
        return $this;
	}
}