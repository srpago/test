DROP DATABASE IF EXISTS db_srpago;
CREATE DATABASE db_srpago;

-- CREACIÓN DE USUARIO PARA NUEVA BASE DE DATOS
CREATE USER IF NOT EXISTS 'usr_srpago'@'localhost' IDENTIFIED BY 'B4s3-SrP4g0!!!';

-- DAMOS TODOS LOS PERMISOS A USUARIO CREADO PARA BASE DE DATOS NUEVA
GRANT ALL PRIVILEGES ON db_srpago.* TO 'usr_srpago'@'localhost';


USE db_srpago;

-- ==============================================
-- =
-- =     CATÁLOGOS DE SISTEMA
-- =
-- ==============================================

CREATE TABLE sepomex (
  id SERIAL NOT NULL AUTO_INCREMENT,
  d_codigo VARCHAR(5) NOT NULL,
  d_asenta VARCHAR(100),
  d_tipo_asenta VARCHAR(50),
  d_mnpio VARCHAR(100),
  d_estado VARCHAR(100),
  d_ciudad VARCHAR(50),
  d_cp VARCHAR(5),
  c_estado VARCHAR(2),
  c_oficina VARCHAR(5),
  c_cp VARCHAR(5),
  c_tipo_asenta VARCHAR(2),
  c_mnpio VARCHAR(3),
  id_asenta_cpcons VARCHAR(4),
  d_zona VARCHAR(6),
  c_cve_ciudad VARCHAR(2),
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  created_by BIGINT(10) NOT NULL DEFAULT 1,
  updated_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_by BIGINT(10) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE sepomex ADD FULLTEXT idx_sepomex_cp (d_codigo, d_mnpio, d_estado);



CREATE TRIGGER bf_u_sepomex
  BEFORE UPDATE ON sepomex
    FOR EACH ROW
    SET NEW.updated_at = now();

-- IMPORTAMOS INFORMACIÓN DE SEPOMEX

LOAD DATA LOCAL INFILE  './sepomex_all_data.txt'
INTO TABLE sepomex 
FIELDS TERMINATED BY '|' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 2 ROWS
(d_codigo, d_asenta, d_tipo_asenta, d_mnpio, d_estado, d_ciudad, d_cp, c_estado, c_oficina, c_cp, c_tipo_asenta, c_mnpio, id_asenta_cpcons, d_zona, c_cve_ciudad);

CREATE TABLE precio_gasolina (
  id SERIAL NOT NULL AUTO_INCREMENT,
  _id VARCHAR(50) NOT NULL DEFAULT '',
  calle VARCHAR(300) NOT NULL DEFAULT '',
  rfc VARCHAR(20) NOT NULL DEFAULT '',
  date_insert VARCHAR(50) NOT NULL DEFAULT '',
  regular DECIMAL(5,2) NOT NULL DEFAULT 0,
  colonia VARCHAR(55) NOT NULL DEFAULT'',
  numeropermiso VARCHAR(50) NOT NULL DEFAULT '',
  fechaaplicacion VARCHAR(25) NOT NULL DEFAULT '',
  permisoid VARCHAR(50) NOT NULL DEFAULT '',
  longitude FLOAT(10,6) NOT NULL DEFAULT '',
  latitude FLOAT(10,6) NOT NULL DEFAULT '',
  premium DECIMAL(5,2) NOT NULL DEFAULT 0,
  razonsocial VARCHAR(200) NOT NULL DEFAULT '',
  codigopostal VARCHAR(6) NOT NULL DEFAULT '',
  dieasel DECIMAL(5,2) NOT NULL DEFAULT 0,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  created_by BIGINT(10) NOT NULL DEFAULT 1,
  updated_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_by BIGINT(10) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE precio_gasolina ADD FULLTEXT idx_codigopostal (codigopostal);

CREATE TRIGGER bf_u_precio_gasolina
  BEFORE UPDATE ON precio_gasolina
    FOR EACH ROW
      SET NEW.updated_at = now();

DELIMITER $$